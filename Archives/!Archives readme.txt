Each txt file contains the urls of previously downloaded files. The program will not re-download these urls to save time.

These archives are only updated and checked when using the auto-update playlist functionality.

These urls will not be removed when deleting a downloaded file, so to do a fresh download delete the corresponding archive file.