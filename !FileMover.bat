@ECHO OFF

echo Hit Enter to Move the downloaded files to their storage location
echo Configure the storage location with MoveLocation.conf

PAUSE

::Read the targetLocation from MoveLocation.conf
SET /p @targetLocation=<c:\JacksHandyDandyYoutubeNicker\Config\MoveLocation.conf

::Cut/Paste the files from !TempStorage to targetLocation, /s = move all subfolders as well
robocopy "c:\JacksHandyDandyYoutubeNicker\!TempStorage\!youtube-dl output" %@targetLocation% /move /s

echo ==========================================
echo ------------------DONE--------------------
echo ==========================================

PAUSE