@ECHO OFF

cd c:\JacksHandyDandyYoutubeNicker

::Check for updates to youtube-dl and donload them if they're available
echo Checking for youtube-dl updates. If an update is found, restart the downloader when updated
youtube-dl -U

echo x
echo x

::Get save the url for later
echo [Enter the url of the video or playlist, make sure the video/playlist is not private,]
SET /P url=[hit enter without entering a url to auto-update downloaded playlists]

echo x
echo x

::Get url of download, default to auto-update playlists if no url is given
IF "%url%"=="" (
	echo auto-update selected, skipping audio/video selection
) ELSE (
	::Set if downloading video or audio-only
	SET /P typeBool=[Enter 1 for video download, 2 for audio only]
	
	echo x
	echo x

	::Set if downloads are sorted into folders
	SET /P downloadsSortedBool=[Enter 1 for automatic sub-folder naming, 2 for no playlist subfolders]

	echo x
	echo x
)

::If downloading a video, prompt for format. Record Format Code in formatString
IF "%typeBool%"=="1" (
	Set /P formatString=[Enter the Format Code of the format you want to download the file as. You can list the available formats using !FormatLister.bat. Leave empty to default to best quality]
)

::If no Format Code is given, default to best quality
IF "%formatString%"=="" (
	SET formatString="best"
)

::If downloading a video, display the Format Code chosen
if "%typeBool%"=="1" (
	echo x
	echo x

	echo set formatString to %formatString%
)

echo x
echo x

IF "%url%"=="" (
	::If no url given, auto update
	GOTO autoLabel
) ELSE (
	IF "%downloadsSortedBool%"=="2" (
		::Not sorting into playlist folders
		GOTO nonSortedLabel
	) ELSE (
		::Sorting into playlist folders
		GOTO sortedLabel
	)
)

:nonSortedLabel
echo -nonSortedLabel-
IF "%typeBool%"=="1" youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigNoSorting.conf -f %formatString% %url% --ignore-errors
IF "%typeBool%"=="2" youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigNoSorting.conf --add-metadata --embed-thumbnail -f bestaudio -x --audio-format mp3 %url% --ignore-errors
GOTO endLabel
	
:sortedLabel
echo -sortedLabel-
IF "%typeBool%"=="1" youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigAutoSort.conf -f %formatString% %url% --ignore-errors
IF "%typeBool%"=="2" youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigAutoSort.conf --add-metadata --embed-thumbnail -f bestaudio -x --audio-format mp3 %url% --ignore-errors
GOTO endLabel

:autoLabel
echo -autoLabel-

::subs------------------
::attempt 1080p
youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigAutoSort.conf -f 137+bestaudio --batch-file="c:\JacksHandyDandyYoutubeNicker\AutoPlaylists\playlistSubConfig.txt" --ignore-errors --write-srt --sub-lang en --download-archive Archives\SubArchive.txt

::default to best available
youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigAutoSort.conf --batch-file="c:\JacksHandyDandyYoutubeNicker\AutoPlaylists\playlistSubConfig.txt" --ignore-errors --write-srt --sub-lang en --download-archive Archives\SubArchive.txt

::Video----------------
::attempt 1080p
youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigAutoSort.conf -f 137+bestaudio --batch-file="c:\JacksHandyDandyYoutubeNicker\AutoPlaylists\playlistVideoConfig.txt" --ignore-errors --download-archive Archives\VideoArchive.txt

::default to best available
youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigAutoSort.conf --batch-file="c:\JacksHandyDandyYoutubeNicker\AutoPlaylists\playlistVideoConfig.txt" --ignore-errors --download-archive Archives\VideoArchive.txt

::Audio----------------
youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigAutoSort.conf --add-metadata --embed-thumbnail -f bestaudio -x --audio-format mp3 --batch-file="c:\JacksHandyDandyYoutubeNicker\AutoPlaylists\playlistAudioConfig.txt" --ignore-errors --download-archive Archives\AudioArchive.txt

GOTO endLabel

:endLabel
echo =====================================================================
echo --------------------------------DONE!--------------------------------
echo =====================================================================

pause