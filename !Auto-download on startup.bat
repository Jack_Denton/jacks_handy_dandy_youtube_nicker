@ECHO OFF

cd c:\JacksHandyDandyYoutubeNicker

::Check for updates to youtube-dl and donload them if they're available
echo Checking for youtube-dl updates. If an update is found, restart the downloader when updated
youtube-dl -U

timeout /t 10 /nobreak > NUL

echo x
echo x

GOTO autoLabel

:autoLabel
echo -autoLabel-

::subs
youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigAutoSort.conf -f best --batch-file="c:\JacksHandyDandyYoutubeNicker\AutoPlaylists\playlistSubConfig.txt" --ignore-errors --write-srt --sub-lang en --download-archive Archives\SubArchive.txt

::Video
youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigAutoSort.conf -f best --batch-file="c:\JacksHandyDandyYoutubeNicker\AutoPlaylists\playlistVideoConfig.txt" --ignore-errors --download-archive Archives\VideoArchive.txt

::Audio
youtube-dl --config-location c:\JacksHandyDandyYoutubeNicker\Config\ConfigAutoSort.conf --add-metadata --embed-thumbnail -f bestaudio -x --audio-format mp3 --batch-file="c:\JacksHandyDandyYoutubeNicker\AutoPlaylists\playlistAudioConfig.txt" --ignore-errors --download-archive Archives\AudioArchive.txt
GOTO endLabel

:endLabel
echo =====================================================================
echo --------------------------------DONE!--------------------------------
echo =====================================================================