Push the windows key and type 'run' and push enter

In the Run window, type 'shell:startup' to open the Startup folder

Paste '!Auto-download on startup - Shortcut' into the Startup folder

On startup, the program will now automatically check all playlists in the Auto-Playlist configuration files for new videos and download them to the locations specified in the Config files