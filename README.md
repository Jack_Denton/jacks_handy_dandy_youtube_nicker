__JacksHandyYoutubeNicker v2.1.3__

Place the JacksHandyDandyYoutubeNicker folder in the top level of your C: drive

Double-click "!Youtube Downloader" and follow the prompts

Private videos and playlists cannot be nicked. If you have a private playlist or video, consider changing it to unlisted to download it.

By default, files will be downloaded to the "!youtube-dl output" folder in your Downloads folder. You can choose to download them into sub-folders that will automatically be created based on playlists and such, or just download them directly to the default output folder. 

You can change the download location by editing the .conf files in the Config folder. There are examples to demonstrate how to configure to download to regular folders, synced cloud folders, and external hard drives. Open them with notepad++ or an IDE like Monodevelop or Visual Studios. Regular Notepad tends to change the file type to .txt without any notice, breaking the program in the process

If you re-download a playlist or single file and the file(s) already exist in the output folder the Nicker will skip them, so don't be afraid to run the program again to update your downloaded playlists with new files.

"!Output Folder Shortcut" will take you to the default output folder in your Downloads folder if it exists. If the folder does not exist it will be created when the nicker is run for the first time

You can set up the program to automatically check playlists for new videos to download by adding the urls to playlistAudioConfig.txt and playlistVideoConfig.txt in the AutoPlaylists folder. When the .txt files are set up just run the program and hit enter when prompted, the nicker will check all the playlists for files that haven't been downloaded yet and download them as video or audio files depending which playlist text file the urls were placed. To speed up this procees files that have already been downloaded will be recorded in AudioArchive.txt and VideoArchive.txt in the Archives folder, the program will not bother to even check the urls if they are recorded here. If you want to do an exhaustive check of the playlists delete these files and run the auto-update again. When used in this way, the nicker will attempt to download in 1080p, then default to the 'best' quality available.

You can set up the program to automatically update configured playlists when the computer boots up. Follow the instructions in the 'Auto-download on startup instructions' folder.

If you set up the program to automatically run on launch, it might be a good idea to make use of !FileMover and the !TempStorage folder. Change the download locations in ConfigNoSorting.conf and ConfigAutoSort.conf to target !TempStorage (there is a pre-written line that just needs to be uncommented). Now all downloads will go to the !TempStorage folder. Set the location the files will be moved to in MoveLocation.conf (a large external harddrive would be best). Now you just have to occasionally run !FileMover to move the files in !TempStorage to your external harddrive.

If you want to download a specific file format, enter the url in "!Format Lister.bat" and take note of the Format Code of the desired format. When prompted for a Format Code in the "!Youtube Downloader.bat" enter the desired code. This feature will probably not work with playlists, only individual videos.

If no Format Code is entered the downloader will default to the best quality available.

Some sites, like crunchyroll, embed different subtitles in their formats. This feature is required to get the correct subtitles in these cases.